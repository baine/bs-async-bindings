type callback('r) = (Js.Nullable.t(Js.Exn.t), Js.Nullable.t('r)) => unit;
type iteratee('a, 'b) = ('a, callback('b)) => unit;

type emptyCallback = Js.Nullable.t(Js.Exn.t) => unit;
type emptyIteratee('a) = ('a, emptyCallback) => unit;

type indexedEmptyIteratee('a) = ('a, int, emptyCallback) => unit;

[@bs.module "async"]
external concat:
  (array('a), iteratee('a, array('b)), callback(array('b))) => unit =
  "";
[@bs.module "async"]
external concatLimit:
  (array('a), int, iteratee('a, array('b)), callback(array('b))) => unit =
  "";
[@bs.module "async"]
external concatSeries:
  (array('a), iteratee('a, array('b)), callback(array('b))) => unit =
  "";

[@bs.module "async"]
external detect: (array('a), iteratee('a, bool), callback('a)) => unit = "";
[@bs.module "async"]
external detectLimit: (array('a), int, iteratee('a, bool), callback('a)) =
  "";
[@bs.module "async"]
external detectSeries: (array('a), iteratee('a, bool), callback('a)) = "";

[@bs.module "async"]
external each: (array('a), emptyIteratee('a), emptyCallback) => unit = "";
[@bs.module "async"]
external eachLimit:
  (array('a), int, emptyIteratee('a), emptyCallback) => unit =
  "";
[@bs.module "async"]
external eachSeries: (array('a), emptyIteratee('a), emptyCallback) => unit =
  "";

[@bs.module "async"]
external eachOf: (array('a), indexedEmptyIteratee('a), emptyCallback) => unit =
  "";
[@bs.module "async"]
external eachOfLimit:
  (array('a), int, indexedEmptyIteratee('a), emptyCallback) => unit =
  "";
[@bs.module "async"]
external eachOfSeries:
  (array('a), indexedEmptyIteratee('a), emptyCallback) => unit =
  "";

[@bs.module "async"]
external every: (array('a), iteratee('a, bool), callback(bool)) => unit =
  "";
[@bs.module "async"]
external everyLimit:
  (array('a), int, iteratee('a, bool), callback(bool)) => unit =
  "";
[@bs.module "async"]
external everySeries:
  (array('a), iteratee('a, bool), callback(bool)) => unit =
  "";

[@bs.module "async"]
external filter:
  (array('a), iteratee('a, bool), callback(array('a))) => unit =
  "";
[@bs.module "async"]
external filterLimit:
  (array('a), int, iteratee('a, bool), callback(array('a))) => unit =
  "";
[@bs.module "async"]
external filterSeries:
  (array('a), iteratee('a, bool), callback(array('a))) => unit =
  "";

[@bs.module "async"]
external groupBy:
  (array('a), iteratee('a, string), callback(Js.Dict.t(array('a)))) =>
  unit =
  "";
[@bs.module "async"]
external groupByLimit:
  (
    array('a),
    int,
    iteratee('a, string),
    callback(Js.Dict.t(array('a)))
  ) =>
  unit =
  "";
[@bs.module "async"]
external groupBySeries:
  (array('a), iteratee('a, string), callback(Js.Dict.t(array('a)))) =>
  unit =
  "";

[@bs.module "async"]
external map: (array('a), iteratee('a, 'b), callback(array('b))) => unit =
  "";
[@bs.module "async"]
external mapLimit:
  (array('a), int, iteratee('a, 'b), callback(array('b))) => unit =
  "";
[@bs.module "async"]
external mapSeries:
  (array('a), iteratee('a, 'b), callback(array('b))) => unit =
  "";

[@bs.module "async"]
external mapValues:
  (Js.Dict.t('a), iteratee('a, 'b), callback(Js.Dict.t('b))) => unit =
  "";
[@bs.module "async"]
external mapValuesLimit:
  (Js.Dict.t('a), int, iteratee('a, 'b), callback(Js.Dict.t('b))) => unit =
  "";
[@bs.module "async"]
external mapValuesSeries:
  (Js.Dict.t('a), iteratee('a, 'b), callback(Js.Dict.t('b))) => unit =
  "";

[@bs.module "async"]
external reduce:
  (array('a), 'b, ('b, 'a, callback('b)) => unit, callback('b)) => unit =
  "";
[@bs.module "async"]
external reduceRight:
  (array('a), 'b, ('b, 'a, callback('b)) => unit, callback('b)) => unit =
  "";

[@bs.module "async"]
external reject:
  (array('a), iteratee('a, bool), callback(array('a))) => unit =
  "";
[@bs.module "async"]
external rejectLimit:
  (array('a), int, iteratee('a, bool), callback(array('a))) => unit =
  "";
[@bs.module "async"]
external rejectSeries:
  (array('a), iteratee('a, bool), callback(array('a))) => unit =
  "";

[@bs.module "async"]
external some: (array('a), iteratee('a, bool), callback(bool)) => unit = "";
[@bs.module "async"]
external someLimit:
  (array('a), int, iteratee('a, bool), callback(bool)) => unit =
  "";
[@bs.module "async"]
external someSeries: (array('a), iteratee('a, bool), callback(bool)) => unit =
  "";
