[@bs.val] external setTimeout: (unit => unit, int) => float = "";

let null = Js.Nullable.null;

let iteratee = (i, cb) => {
  setTimeout(() => cb(null, Js.Nullable.return(i + 1)), i * 2000) |> ignore;
  ();
};
let callback = (id, err, items) => Js.log3(id, err, items);

/* these two should finish at the same time */
AsyncBindings.map([|1, 2, 3|], iteratee, callback("map"));
AsyncBindings.mapLimit([|1, 2, 3|], 1, iteratee, callback("mapLimit 1"));

/* then this one */
AsyncBindings.mapLimit([|1, 2, 3|], 2, iteratee, callback("mapLimit 2"));

/* finally these two at the same time */
AsyncBindings.mapLimit([|1, 2, 3|], 3, iteratee, callback("mapLimit 3"));
AsyncBindings.mapSeries([|1, 2, 3|], iteratee, callback("mapSeries"));
